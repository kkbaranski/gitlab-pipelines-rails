# Example Gitlab pipeline configuration for Rails projects

In this article, I'd like to show an example configuration of Gitlab Pipeline for Rails projects.
I assume that you have at least basic knowledge about Gitlab pipelines: how it works, how to define stages, jobs, etc.
If not, please read [Getting Started](https://docs.gitlab.com/ee/ci/quick_start/README.html) first.

## Goals

Objectives we would like to achieve are:

- Analyze static code in terms of style, security, complexity, etc.
- Check vulnerability and deprecations of gems.
- Run tests and check the coverage of the code.
- Generate ERD of the current version of the database.
- Present the results in an accessible form.
- Deploy the code in the proper place.
- Avoid redundancy.
- Make everything above to run as fast as possible.

## Gems

In order to reach our objectives, we need a couple of libraries.
This is our gem team:

1. [Rubocop Rails](https://github.com/rubocop-hq/rubocop-rails) - static code analyzer and code formatter.
1. [Brakeman](https://brakemanscanner.org/) - static analysis tool for security vulnerabilities.
1. [Dawnscanner](https://dawnscanner.org/) - source code scanner also for security issues.
1. [Bundler Audit](https://github.com/rubysec/bundler-audit) - checks for vulnerable and insecure versions of gems.
1. [RSpec](http://rspec.info/) - testing framework.
1. [Rails ERD](https://voormedia.github.io/rails-erd/) - generates diagrams based on your Active Record models. 
1. [SimpleCov](https://github.com/colszowka/simplecov) - code coverage analysis tool.

# Gitlab Pipeline Script

## Stages

At the beginning let's split our goals into stages.
In our example we will define 4 ones:

1. `build`: in this stage, we will check if the code style meets the rules (with the help of Rubocop) and if assets could be precompiled without errors.
1. `test`: this stage will be for testing our application in every possible way (unit tests, coverage, security issues, etc.)
1. `report`: here, we will present the results of the previous stage in friendly (?) form.
1. `deploy`: and the last one, as the name suggests, will be reserved for deployment.

So here is how our stages configuration looks like:

```yaml
stages:
  - build
  - test
  - report
  - deploy
```

## Parent jobs

To avoid some of the redundancy, Gitlab gives us a before_script option, which allows us to configure server, database, install gems, etc. before every job.
But some jobs don't require e.g. a configured database, so to speed up preparing an environment for these jobs, we can create hidden jobs with different options and every task will inherit from them.



## Base hidden job

```yaml
.base:
  image: ruby:2.6.3
  before_script:
    - gem install bundler --no-document
    - bundle install --jobs $(nproc) "${FLAGS[@]}"
```

## Base hidden job with configured database

To not repeat image option, we will inherit this job from `.base`. Unfortunately there is no option to just append script to already defined `before_script` in `.base` job, so we have to repeat 2 lines:

```yaml
.base_db:
  extends: .base
  services:
    - postgres:latest
  variables:
    POSTGRES_DB: app-name_gitlab
    POSTGRES_USER: app-name
    POSTGRES_PASSWORD: ''
    RAILS_ENV: gitlab
  before_script:
    - apt-get update -qq && apt-get install -yqq nodejs
    - gem install bundler --no-document
    - bundle install --jobs $(nproc) "${FLAGS[@]}" --path=vendor
    - bundle exec rake db:setup
```

Instead of `extends` keyword, it is possible to use [YAML Anchors](https://docs.gitlab.com/ee/ci/yaml/README.html#anchors) but above approach is, in my opinion, more readable and elegant.



## Rubocop

`Rubocop` doesn't require connection with the database, so we can inherit from just `.base`.
Configuration of this job is very simple and looks like this:

```yaml
build:rubocop:
  extends: .base
  stage: build
  script:
    - bundle exec rubocop
```

## Assets precompile

`rake assets:precompile` requires more things eg. `nodejs` installed, that's why we inherit from `.base_db` in this case:

```yaml
build:assets_precompile:
  extends: .base_db
  stage: build
  script:
    - bundle exec rake assets:precompile
```

## Bundle audit

Failing this job, shouldn't fail the whole pipeline, because it should be just an alert poining out that some of gems require paying attention on them, so we need to set `allow_failure` flag, to avoid this.

```yaml
test:bundle_audit:
  extends: .base
  allow_failure: true
  script:
    - bundle exec bundle audit check --update
```


## Brakeman and Dawnscanner

`Brakeman` and `Dawnscanner` gems generate reports, so let's add them to the artifacts ([read more about artifacts](https://docs.gitlab.com/ee/ci/yaml/README.html#artifacts)).
The artifacts should be generated especially when the reports fails, so we have to add an option `when: always` to ensure it.
To not run this job on every single commit, we can limit this job e.g. to only `master` branch.
So complete configuration of these jobs look like:

```yaml
test:brakeman:
  extends: .base
  stage: test
  allow_failure: true
  artifacts:
    name: brakeman_report
    when: always
    paths:
      - brakeman/
  only:
    - master
  script:
    - bundle exec brakeman --format html -o brakeman/index.html
    
test:dawnscanner:
  extends: .base
  stage: test
  allow_failure: true
  artifacts:
    name: dawnscanner_report
    when: always
    paths:
      - dawnscanner
  only:
    - master
  script:
    - mkdir dawnscanner
    - bundle exec dawn --html -zF dawnscanner/index.html .
```

## Tests

Test of course require connection with the database, so this time we have to inherit from `.base_db`.
`Rspec` gem combined with `SimpleCov` (HTML version) generates HTML coverage report which can be also added to artifacts.

```yaml
test:rspec:
  extends: .base_db
  stage: test
  artifacts:
    name: coverage_report
    paths:
      - coverage/
  script:
    - bundle exec rspec
```

## Entity Relationship Diagram (ERD)

With Gitlab Pipeline we can generate and keep always fresh diagram of our database (e.g. in production) handy.
To achieve it, we will use [Rails ERD](https://voormedia.github.io/rails-erd/) gem.
To use this feature we need to install `graphviz` package first and as in previous reports we can move generated `pdf` to artifacts.

```yaml
test:erd:
  extends: .base_db
  stage: test
  allow_failure: true
  artifacts:
    name: erd
    paths:
      - erd.pdf
  only:
    - master
  script:
    - apt-get -o dir::cache::archives="$APT_CACHE_DIR" install -y -qq graphviz
    - bundle exec rake erd
```

## Pages

In order to present all above reports in an accessible form, we can use [pages](https://docs.gitlab.com/ee/ci/yaml/README.html#pages).
Then e.g. we can add links to particular reports in `README` or on another page.
But keep in mind that Gitlab Pages are accesible for everyone, no matter if project is public or private, so this is good for open sourse projects only, becase e.g. from coverage report we can copy whole sourse code.
For now, there is not possible to add an authentication to the Pages, there is an open issue about that ([link](https://gitlab.com/gitlab-org/gitlab-pages/issues/22)).
The job below will create following endpoints:

- `<username>`.gitlab.io/`<projectname>`/brakeman
- `<username>`.gitlab.io/`<projectname>`/coverage
- `<username>`.gitlab.io/`<projectname>`/dawnscanner
- `<username>`.gitlab.io/`<projectname>`/erd.pdf

```yaml
pages:
  extends: .base
  stage: report
  allow_failure: true
  dependencies:
    - test:brakeman
    - test:dawnscanner
    - test:erd
    - test:rspec
  artifacts:
    name: my_app_artifacts
    paths:
      - public/brakeman
      - public/coverage
      - public/dawnscanner
      - public/erd.pdf
  only:
    - master
  script:
    - mv brakeman public/ || true
    - mv coverage public/ || true
    - mv dawnscanner public/ || true
    - mv erd.pdf public/ || true

```

## Deployment

In Gitlab Pipeline we can also perform deployments.
In our example we use 2 types of deployment process configurations: automatic (on staging) and manual (on production).
When we push some code to staging branch, job with deployment script will be triggered automaticaly as usual pipeline job:

```yaml
deploy:staging:
  extends: .base
  stage: deploy
  only:
    - staging
  script:
    - echo "Deploying to Staging..."
```

In case of deployment to production, we will use `when: manual` option, so to trigger this job, we need to click "play" button near the job name on Pipeline graph (or on the Jobs page):

![play](images/play.png)

We can also add `environment` options, so after deploy the link to production page will appear in various places in GitLab which when clicked take you to the defined URL.
The final configuration of `deploy:production` looks like:

```yaml
deploy:production:
  extends: .base
  stage: deploy
  when: manual
  environment:
    name: production
    url: https://example.com
  only:
    - production
  script:
    - echo "Deploying to Production..."
```

# Improvements

## Cache

Gitlab offers caching system which can be used to significantly speed up running time of jobs.
In our example we will be caching gems and Linux packages.
To define cache, we have to add the option to our `.base` job:

```yaml
cache:
  key: gems_and_packages
  paths:
    - apt-cache/
    - vendor/ruby
```

We need to also update installing gems and packages to use the cache, so our parent jobs should now look like:

```yaml
.base:
  image: ruby:2.6.3
  cache:
    key: gems_and_packages
    paths:
      - apt-cache/
      - vendor/ruby
  before_script:
    - gem install bundler --no-document
    - bundle install --jobs $(nproc) "${FLAGS[@]}" --path=vendor

.base_db:
  extends: .base
  services:
    - postgres:latest
  variables:
    POSTGRES_DB: my_app_gitlab
    POSTGRES_USER: my_app
    POSTGRES_PASSWORD: ''
    RAILS_ENV: gitlab
  before_script:
    - export APT_CACHE_DIR=`pwd`/apt-cache && mkdir -pv $APT_CACHE_DIR
    - apt-get update -qq && apt-get -o dir::cache::archives="$APT_CACHE_DIR" install -yqq nodejs
    - gem install bundler --no-document
    - bundle install --jobs $(nproc) "${FLAGS[@]}" --path=vendor
    - bundle exec rake db:setup
```

By my tests, cache improves running time of every job by 1:09 minutes on average.

## Cache policy

With the foregoing configuration, cache is pulled at the beginning of every job, and at the end, cache is built from scratch and saved.
We can improve this proces using [cache policy option](https://docs.gitlab.com/ee/ci/yaml/README.html#cachepolicy).
Note that it's enough to build a cache once through the whole pipeline, and in every job just to pull it.
To set this behaviour, let's update cache option in `.base` job:

```yaml
cache:
  key: gems_and_packages
  paths:
    - apt-cache/
    - vendor/ruby
  policy: pull
```

From now, cache is only pulled and extracted at the beggining of every job, but is not not built anywhere.
Let's add building the cache e.g. to `build:rubocop` job:

```yaml
build:rubocop:
  extends: .base
  stage: build
  cache:
    policy: pull-push
  script:
    - bundle exec rubocop
```

In my experiences, this improvement speeds up every job by 1:41 minutes on average in relation to version without cache at all.

## Number of stages

In Gitlab Pipeline there is no possible to share Docker machines between stages, so machines have to be pulled and configured every time from scratch for every job.
Jobs in one stage can run in parallel, whereas jobs from different stages don't, so in order to speedup the whole pipeline, we should have as little stages as possible.
In our example we could move jobs from `build` stage into `test` thereby decreasing number of stages by 1, but then, if somebody e.g. forgot to remove `binding.pry` from the code, in our example `Rubocop` will fail already in the first stage and pipeline won't move to next one, otherwise whole pipeline will hang on `Rspec` job until timeout. I chose the lesser evil in this case.
Speedup of this improvement is equal to the longest job in removed stage or even more, because sometimes jobs don't start simultaneusly.

## Smaller Docker image

Let's take a closer look at the `pages` job. The only work of this task is to move artifacts to another place (`public/` directory).
Note that the only command needed by this job is `mv`, which is in every Linux distro.
So instead of pulling a big docker image with ruby installed, we can simply pull the smallest possible image.
Please also note, that we are not using cache at all, so this feature can be also disabled.
Let's do that and create `.base_minimal` hidden job, with Alpine Linux as image (only 5 MB size!) and cache disabled at all:

```yaml
.base_minimal:
  image: alpine:latest
  cache: {}
```

And then, update the `pages` job:

```yaml
pages:
  extends: .base_minimal
  stage: report

  ...
```

## Final version

Below you can see the final version of Gitlab Pipeline configuration file with improvements mentioned above:

```yaml
stages:
  - build
  - test
  - report
  - deploy

#=========================================== Base ============================================#
.base:
  image: ruby:2.6.3
  cache:
    key: gems_and_packages
    paths:
      - apt-cache/
      - vendor/ruby
    policy: pull
  before_script:
    - gem install bundler --no-document
    - bundle install --jobs $(nproc) "${FLAGS[@]}" --path=vendor

#======================================= Base with DB ========================================#
.base_db:
  extends: .base
  services:
    - postgres:latest
  variables:
    POSTGRES_DB: my_app_gitlab
    POSTGRES_USER: my_app
    POSTGRES_PASSWORD: ''
    RAILS_ENV: gitlab
  before_script:
    - export APT_CACHE_DIR=`pwd`/apt-cache && mkdir -pv $APT_CACHE_DIR
    - apt-get update -qq && apt-get -o dir::cache::archives="$APT_CACHE_DIR" install -yqq nodejs
    - gem install bundler --no-document
    - bundle install --jobs $(nproc) "${FLAGS[@]}" --path=vendor
    - bundle exec rake db:setup

#========================================== Minimal ==========================================#
.base_minimal:
  image: alpine:latest
  cache: {}

#===================================== Assets Precompile =====================================#
build:assets_precompile:
  extends: .base_db
  stage: build
  script:
    - bundle exec rake assets:precompile

#========================================== Rubocop ==========================================#
build:rubocop:
  extends: .base
  stage: build
  cache:
    policy: pull-push
  script:
    - bundle exec rubocop

#========================================= Brakeman ==========================================#
test:brakeman:
  extends: .base
  stage: test
  allow_failure: true
  artifacts:
    name: brakeman_report
    when: always
    paths:
      - brakeman/
  only:
    - master
  script:
    - bundle exec brakeman --format html -o brakeman/index.html


#======================================= Bundle Audit ========================================#
test:bundle_audit:
  extends: .base
  allow_failure: true
  script:
    - bundle exec bundle audit check --update

#======================================== Dawnscanner ========================================#
test:dawnscanner:
  extends: .base
  stage: test
  allow_failure: true
  artifacts:
    name: dawnscanner_report
    when: always
    paths:
      - dawnscanner
  only:
    - master
  script:
    - mkdir dawnscanner
    - bundle exec dawn --html -zF dawnscanner/index.html .

#============================================ ERD ============================================#
test:erd:
  extends: .base_db
  stage: test
  allow_failure: true
  artifacts:
    name: erd
    paths:
      - erd.pdf
  only:
    - master
  script:
    - apt-get -o dir::cache::archives="$APT_CACHE_DIR" install -y -qq graphviz
    - bundle exec rake erd

#=========================================== Rspec ===========================================#
test:rspec:
  extends: .base_db
  stage: test
  artifacts:
    name: coverage_report
    paths:
      - coverage/
  script:
    - bundle exec rspec

#=========================================== Pages ===========================================#
pages:
  extends: .base_minimal
  stage: report
  allow_failure: true
  dependencies:
    - test:brakeman
    - test:dawnscanner
    - test:erd
    - test:rspec
  artifacts:
    name: time_tracking_artifacts
    paths:
      - public/brakeman
      - public/coverage
      - public/dawnscanner
      - public/erd.pdf
  only:
    - master
  script:
    - mv brakeman public/ || true
    - mv coverage public/ || true
    - mv dawnscanner public/ || true
    - mv erd.pdf public/ || true

#===================================== Deploy to Staging =====================================#
deploy:staging:
  extends: .base
  stage: deploy
  only:
    - staging
  script:
    - echo "Deploying to Staging..."

#==================================== Deploy to Production ===================================#
deploy:production:
  extends: .base
  stage: deploy
  when: manual
  environment:
    name: production
    url: https://example.com
  only:
    - production
  script:
    - echo "Deploying to Production..."

```

Below you can see how Pipeline graphs look depending on different branches:

### Branches not specified in config file (e.g. `feature/1`)

![feature/1](images/feature1.png)

### `master`

![master](images/master.png)

### `staging`

![staging](images/staging.png)

### `production`

![production](images/production.png)


## Summary

[requires expansion]

- I showed one of the many different ways to configure it
- Gitlab Pipeline is more powerful and flexible
- You can read about other options [here](https://docs.gitlab.com/ee/ci/yaml/README.html)

