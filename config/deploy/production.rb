# frozen_string_literal: true

set :stage, :production
set :rails_env, :production
set :branch, :production
set :deploy_to, '/var/proj/gitlab-test/production'
server 'dev03.n0p.io', user: 'deployer', roles: %w[app db web]
