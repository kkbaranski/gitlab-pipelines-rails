# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock '~> 3.11.0'

set :application, 'my_app_name'
set :repo_url, 'git@gitlab.com:kkbaranski/gitlab-pipelines-rails.git'

set :deploy_via, :copy

set :rvm_type, :system
set :rvm_ruby_version, '2.6.3'
set :migration_role, :db
set :passenger_restart_with_touch, true

append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads'
